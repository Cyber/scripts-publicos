udhcpc
mkdir dpkg libbz2 zlib libacl libselinux liblzma libcrypt gcc libgcc tar libc6 libpcre
wget "http://ftp.br.debian.org/debian/pool/main/d/dpkg/dpkg_1.20.12_i386.deb" -O dpkg/dpkg.deb
wget "http://ftp.br.debian.org/debian/pool/main/b/bzip2/libbz2-1.0_1.0.8-4_i386.deb" -O libbz2/libbz2.deb
wget "http://ftp.br.debian.org/debian/pool/main/z/zlib/zlib1g_1.2.11.dfsg-2+deb11u2_i386.deb" -O zlib/zlib.deb
wget "http://ftp.br.debian.org/debian/pool/main/a/acl/libacl1_2.2.53-10_i386.deb" -O libacl/libacl.deb
wget "http://ftp.br.debian.org/debian/pool/main/libs/libselinux/libselinux1_3.1-3_i386.deb" -O libselinux/libselinux.deb
wget "http://ftp.br.debian.org/debian/pool/main/x/xz-utils/liblzma5_5.2.5-2.1~deb11u1_i386.deb" -O liblzma/liblzma.deb
wget "http://ftp.br.debian.org/debian/pool/main/libx/libxcrypt/libcrypt1_4.4.18-4_i386.deb" -O libcrypt/libcrypt.deb
wget "http://ftp.br.debian.org/debian/pool/main/g/gcc-10/gcc-10-base_10.2.1-6_i386.deb" -O gcc/gcc.deb
wget "http://ftp.br.debian.org/debian/pool/main/g/gcc-10/libgcc-s1_10.2.1-6_i386.deb" -O libgcc/libgcc.deb
wget "http://ftp.br.debian.org/debian/pool/main/t/tar/tar_1.34+dfsg-1_i386.deb" -O tar/tar.deb
wget "http://ftp.br.debian.org/debian/pool/main/g/glibc/libc6_2.31-13+deb11u4_i386.deb" -O libc6/libc6.deb
wget "http://ftp.br.debian.org/debian/pool/main/p/pcre2/libpcre2-8-0_10.36-2+deb11u1_i386.deb" -O libpcre/libpcre.deb
#for i in {dpkg,libbz2,zlib,libacl,libselinux,liblzma,libcrypt,gcc,libgcc,tar,libc6}
for i in $(echo -en "dpkg\nlibbz2\nzlib\nlibacl\nlibselinux\nliblzma\nlibcrypt\ngcc\nlibgcc\nlibc6\ntar\nlibpcre")
do
cd $i
ar -x $i*
rm $i*
xz -d data.tar.xz
cd /
tar xvf /root/$i/data.tar
rm -r /root/$i/
cd /root/
done